var exports = {};


function showDamage() {
    var spellText = document.getElementById("spellId").value;
    var damage = exports.damage(spellText);
    document.getElementById("damageId").innerHTML = String(damage);
}

exports.damage = function (spellString) {
    // Deklaracja wartości poszczególnych elementów zaklęcia
    var spellFe = 1;
    var spellJe = 2;
    var spellJee = 3;
    var spellAin = 3;
    var spellDai = 5;
    var spellNe = 2;
    var spellAi = 2;

    var temp2SpellString;
    var searchFeCount = 0;
    var tempDamage = [];
    var damage = 0;
    var tempSearch = 0;
    var searchAiCount = 0;
    var allAi = [];

    // Funkcja do sprawdzania zaklęcia - czy w danym fragmencie jest jeden z elementów
    function spellStringSub(spell, start, end) {
        return spell.substring(start, end);
    }

    // Funkcja do sprawdzenia poprawności zaklęcia
    function isSpellValid(spell) {
        var tempSearchFe = spell.indexOf("fe");
        while (tempSearchFe >= 0) {
            searchFeCount++;
            tempSearchFe = spell.indexOf("fe", tempSearchFe + 1);
        }
        if (searchFeCount === 1) {
            return true;
        } else return false;
    }

    // Funkcja znajdująca najbardziej opłacalne zaklęcie i zwracająca obrażenia
    function getBestDamage() {
        var bestDamage = 0;
        for (var i = 0; i < allAi.length; i++) {
            if (tempDamage[i] < 0) {
                tempDamage[i] = 0;
            }
            if (bestDamage < tempDamage[i]) {
                bestDamage = tempDamage[i];
            }
        }
        return bestDamage;
    }

    if (isSpellValid(spellString)) {
        var searchFe = spellString.search("fe");

        var indexAi = spellString.indexOf("ai");
        // Znalezienie wszystich możliwych zakończeń zaklęć
        while (indexAi != -1) {
            allAi.push(indexAi);
            tempDamage[searchAiCount] = spellFe + spellAi;
            searchAiCount++;
            indexAi = spellString.indexOf("ai", indexAi + 1);
        }
        // Sprawdzenie różnych wersji zaklęć
        for (var i = 0; i < allAi.length; i++) {
            var tempSearchStart = 2;
            var tempSearchEnd = 2;
            var tempSpellString = spellString.substring(searchFe, allAi[i]);
            while (tempSearchEnd < tempSpellString.length) {
                temp2SpellString = spellStringSub(tempSpellString, tempSearchStart, tempSearchEnd + 3);
                var searchDai = temp2SpellString.search("dai");
                if (searchDai != -1) {
                    tempDamage[i] =  tempDamage[i] + spellDai;
                    tempSearchStart = tempSearchStart + 3;
                    tempSearchEnd = tempSearchEnd + 3;
                    temp2SpellString = spellStringSub(tempSpellString, tempSearchStart, tempSearchEnd + 2);
                    var searchNe = temp2SpellString.search("ne");
                    if (searchNe != -1) {
                        tempDamage[i] += spellNe;
                        tempSearchStart += 2;
                        tempSearchEnd += 2;
                        tempSearch = 1;
                    }else tempSearch = 1;

                }
                if (tempSearch === 0) {
                    temp2SpellString = spellStringSub(tempSpellString, tempSearchStart, tempSearchEnd + 2);
                    var searchAi = temp2SpellString.search("ai");
                    temp2SpellString = spellStringSub(tempSpellString, tempSearchStart + 2, tempSearchEnd + 4);
                    var searchNe = temp2SpellString.search("ne");
                    if (searchAi != -1 && searchNe != -1) {
                        tempDamage[i] += spellAi + spellNe;
                        tempSearchStart += 6;
                        tempSearchEnd += 6;
                        tempSearch = 1;
                    }
                }
                if (tempSearch === 0) {
                    temp2SpellString = spellStringSub(tempSpellString, tempSearchStart, tempSearchEnd + 3);
                    var searchAin = temp2SpellString.search("ain");
                    if (searchAin != -1) {
                        tempDamage[i] += spellAin;
                        tempSearchStart += 3;
                        tempSearchEnd += 3;
                        tempSearch = 1;
                    }
                }
                if (tempSearch === 0) {
                    temp2SpellString = spellStringSub(tempSpellString, tempSearchStart, tempSearchEnd + 2);
                    var searchAi = temp2SpellString.search("ai");
                    if (searchAi != -1) {
                        tempDamage[i] += spellAi;
                        tempSearchStart += 2;
                        tempSearchEnd += 2;
                        tempSearch = 1;
                    }
                }
                if (tempSearch === 0) {
                    temp2SpellString = spellStringSub(tempSpellString, tempSearchStart, tempSearchEnd + 3);
                    var searchJee = temp2SpellString.search("jee");
                    if (searchJee != -1) {
                        tempDamage[i] += spellJee;
                        tempSearchStart += 3;
                        tempSearchEnd += 3;
                        tempSearch = 1;
                    }
                }
                if (tempSearch === 0) {
                    temp2SpellString = spellStringSub(tempSpellString, tempSearchStart, tempSearchEnd + 2);
                    var searchJe = temp2SpellString.search("je");
                    if (searchJe != -1) {
                        tempDamage[i] += spellJe;
                        tempSearchStart += 3;
                        tempSearchEnd += 3;
                        tempSearch = 1;
                    }
                }
                if (tempSearch === 0) {
                    tempSearchStart++;
                    tempSearchEnd++;
                    tempDamage[i] =  tempDamage[i] - 1;
                }
                tempSearch = 0;
            }
        }
        damage = getBestDamage();
    }
    else {
        damage = 0;
    }
    return damage;
}
